-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2016 at 06:39 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hkproject1`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaign`
--

CREATE TABLE `campaign` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `html` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campaign`
--

INSERT INTO `campaign` (`id`, `name`, `html`) VALUES
(1, 'USA', '<div class="intro">\r\n			<h1> Projects By Category: Americas</h1>	\r\n        Stay up to date with project updates from the Americas|/projects/category/americas category.\r\n      </div>'),
(2, 'East and North Asia', '<div class="intro">\r\n			<h1> Projects By Category: East and North Asia</h1>	\r\n        Stay up to date with project updates from the East and North Asia|/projects/category/east-and-north-asia category.\r\n      </div>'),
(3, 'European Union', '<div class="intro">\r\n			<h1> Projects By Category: European Union</h1>	\r\n        Stay up to date with project updates from the European Union|/projects/category/european-union category.\r\n      </div>'),
(4, 'Europe', '<div class="intro">\r\n			<h1> Projects By Category: Europe</h1>	\r\n        Stay up to date with project updates from the Europe|/projects/category/europe category.\r\n      </div>'),
(5, 'Middle East and Africa', '<div class="intro">\r\n			<h1> Projects By Category: Middle East and Africa</h1>	\r\n        Stay up to date with project updates from the Middle East and Africa|/projects/category/middle-east-and-africa category.\r\n      </div>'),
(6, 'Asia and Australasia', '<div class="intro">\r\n			<h1> Projects By Category: South Asia and Australasia</h1>	\r\n        Stay up to date with project updates from the South Asia and Australasia|/projects/category/south-asia-and-australasia category.\r\n      </div>');

-- --------------------------------------------------------

--
-- Table structure for table `signups`
--

CREATE TABLE `signups` (
  `email` text NOT NULL,
  `firstname` char(100) NOT NULL,
  `lastname` char(100) NOT NULL,
  `optin` tinyint(1) NOT NULL,
  `timerequested` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `signups`
--

INSERT INTO `signups` (`email`, `firstname`, `lastname`, `optin`, `timerequested`) VALUES
('chaba@sharepoint.asia', 'H'':kum', 'last.''K', 1, '2016-03-22 05:29:46'),
('hemaant12@share.com', 'H'':mk', 'kum', 1, '2016-03-22 05:07:31'),
('hemant12@share.com', 'H''mk', 'kum', 1, '2016-03-22 05:05:42'),
('hemantup@gmail.com', 'hemant', 'kumar', 1, '2016-03-22 05:26:26'),
('smantha@sharepoint.asia', 'H'''':,''kuma', 'las;', 1, '2016-03-22 05:17:23'),
('somechinessename@sharepoint.asia', 'å›žé“äºº', 'å•ç¥–å›žé“äºº', 1, '2016-03-22 05:33:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `campaign`
--
ALTER TABLE `campaign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signups`
--
ALTER TABLE `signups`
  ADD PRIMARY KEY (`email`(256));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
