<?php

class DbHandler
{

    private $conn;

    function __construct()
    {
        require_once dirname(__FILE__) . '/connect.php';
        // opening db connection
        $db = new dbConnect();
        $this->conn = $db->connect();
    }

    public function isEmailNew($email)
    {
        $r = $this->conn->query("SELECT email FROM signups where email = '" . $email . "'   Limit 1") or die($this->conn->error . __LINE__);
        //   return $r->num_rows == 0  ; // this works in local xmapp, but fails on goDaddy hosting
        $result = $r->fetch_assoc();
        return empty($result);
    }

    public function getCategoriesForDropDown()
    {
        $results = [];
        $r = $this->conn->query('SELECT id, name FROM campaign') or die($this->conn->error . __LINE__);


//return $results  =  $r->fetch_all(); // this works in local xmapp, but fails on goDaddy hosting 

        while ($rr = $r->fetch_assoc()) {
            array_push($results, $rr);
        }
        return $results;

    }

    public function insertIntoTable($email, $firstname, $lastname)
    {

        $email = $this->conn->real_escape_string($email);
        $firstname = $this->conn->real_escape_string($firstname);
        $lastname = $this->conn->real_escape_string($lastname);
        $query = "INSERT INTO signups (email ,  firstname , lastname, optin) VALUES ('" . $email . "', '" . $firstname . "', '" . $lastname . "', '1')";

        $r = $this->conn->query($query) or die($this->conn->error . __LINE__);

    }


}

/**
 * rough work
 *
 * $db = new DbHandler();
 * $data = $db->getCategoriesForDropDown();
 *
 * echo  json_encode($data);
 *
 *
 * $db = new DbHandler();
 * $data = $db->isEmailNew('hemantup@gmail.com');
 *
 * echo  json_encode($data);
 */

?>
