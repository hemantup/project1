<?php
error_reporting(E_ALL);
# Check is SESSION started
if ((function_exists('session_status') && session_status() !== PHP_SESSION_ACTIVE) || !session_id()) {
    session_start();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Project #1 - Registration + Thank You Page">
    <meta property="og:description" content="Project #1 - Registration + Thank You Page"/>
    <meta property="og:title" content="Fort Hays State University Pharmaceutical Research and Manufacturers"/>
    <meta property="og:image"
          content="https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/logos/Fort_Hays_State_University.jpg"/>
    <meta property="og:image:width" content="250"/>
    <meta property="og:image:height" content="250"/>

    <meta property="og:url" content="https://www.fhsu.edu/"/>
    <meta property="og:site_name" content="Fort Hays State University Pharmaceutical Research and Manufacturers"/>
    <meta name="classification" content="Project #1, Registration, Thank You Page"/>
    <meta name="subject" content="Project #1 - Registration + Thank You Page"/>
    <meta name="keywords" content="Project #1, Registration, Thank You Page"/>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

    <title>Fort Hays State University Pharmaceutical Research and Manufacturers</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href="metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script type="text/javascript">
        function IsValidEmail() {
            return $("#email").val().match(/^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i);
        }

        function IsValidFName() {
            return $("#firstname").val().match(/^.{3,100}$/);
        }

        function IsValidLName() {
            return $("#lastname").val().match(/^.{3,100}$/);
        }

        function IsOptInChecked() {
            return $("#optin").is(':checked');
        }

        function IsCapAnswered() {
            return $("#answer").val().match(/^\d{1,2}$/);
        }
        function ApplycssValidation() {

            if (IsValidEmail()) {
                $("#email").parent().parent().addClass("has-success");
                $("#email").parent().parent().removeClass("has-error");

            }
            else {
                $("#email").parent().parent().addClass("has-error");
                $("#email").parent().parent().removeClass("has-success");

            }


            if (IsValidFName()) {
                $("#firstname").parent().parent().addClass("has-success");
                $("#firstname").parent().parent().removeClass("has-error");

            }
            else {
                $("#firstname").parent().parent().addClass("has-error");
                $("#firstname").parent().parent().removeClass("has-success");

            }

            if (IsValidLName()) {
                $("#lastname").parent().parent().addClass("has-success");
                $("#lastname").parent().parent().removeClass("has-error");

            }
            else {
                $("#lastname").parent().parent().addClass("has-error");
                $("#lastname").parent().parent().removeClass("has-success");

            }


            if (IsOptInChecked()) {
                $("#optin").parent().parent().addClass("has-success");
                $("#optin").parent().parent().removeClass("has-error");

            }
            else {
                $("#optin").parent().parent().addClass("has-error");
                $("#optin").parent().parent().removeClass("has-success");


            }
            if (IsCapAnswered()) {
                $("#answer").parent().parent().addClass("has-success");
                $("#answer").parent().parent().removeClass("has-error");

            }
            else {
                $("#answer").parent().parent().addClass("has-error");
                $("#answer").parent().parent().removeClass("has-success");

            }

            switchSubmit();
        }

        function switchSubmit() {
            if (IsValidEmail() && IsValidFName() && IsValidLName() && IsOptInChecked() && IsCapAnswered()) {
                $("#submitButton").addClass("btn-primary");
                $("#submitButton").removeClass("btn-danger");
            }
            else {
                $("#submitButton").addClass("btn-danger");
                $("#submitButton").removeClass("btn-primary");
            }

        }

        $(document).ready(function () {


            $("#submitButton").click(function () {
                $("#myFormToSubmitOptIn").submit();
            });

            $("#email").keyup(function () {
                ApplycssValidation();
            });
            $("#firstname").keyup(function () {
                ApplycssValidation();
            });

            $("#lastname").keyup(function () {
                ApplycssValidation();
            });


            $("#optin").click(function () {
                ApplycssValidation();
            });

            $("#answer").keyup(function () {
                ApplycssValidation();
            });


            $("#reloadCaptcha").click(function () {

                $("#myCaptchaImage").attr("src", "captcha.htm?randomToRefresh=" + (Math.random() * 10));
                $("#answer").val("");
                ApplycssValidation()

            });

            $("#myResetButton").click(function () {

                $("#email").val("");
                $("#firstname").val("");
                $("#lastname").val("");
                $("#optin").removeAttr('checked');
                $('#reloadCaptcha').click();
            });
        });

    </script>
    <style>
        .div-table {
            display: table;
            width: auto;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /*cellspacing:poor IE support for  this*/
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-col {
            float: left; /*fix for  buggy browsers*/
            display: table-column;
            width: 200px;
            background-color: #ccc;
        }
    </style>

</head>

<body>


<div id="fb-root"></div>
<script>


    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=502998669826426&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


<div id="wrapper">

    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation" role="navigation" style="margin-bottom: 0">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="sale.htm">Fort Hays State University Pharmaceutical Research and
                Manufacturers</a>

        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a href="//sharepoint.asia/hk">About Us</a>
            </li>
            <li class="dropdown">
                <div class="fb-share-button" data-layout="button" data-href="//sharepoint.asia/hk"></div>
            </li>
        </ul>
        <div class="navbar-inverse sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">

                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">

                        <!-- /input-group -->
                    </li>


                    <li>
                        <script type="text/javascript"
                                src="http://je.revolvermaps.com/2/1.js?i=46q2wxj4e8j&amp;s=220&amp;m=0&amp;v=true&amp;r=false&amp;b=000000&amp;n=false&amp;c=ff0000"
                                async="async"></script>
                    </li>
                    <li>
                        <div class="fb-like" data-href="https://www.facebook.com/AuctionCell" data-layout="button_count"
                             data-action="like" data-show-faces="true" data-share="false"></div>
                    </li>
                    <li><a rel="external nofollow" target="_blank" href="http://info.flagcounter.com/hHTJ">
                            <img width="130.2" height="112" rel="external nofollow"
                                 src="http://s04.flagcounter.com/count/hHTJ/bg_FFFFFF/txt_000000/border_CCCCCC/columns_2/maxflags_12/viewers_0/labels_1/pageviews_1/flags_1/"
                                 alt="Flag Counter" border="0"></a></li>


                    <li>
                        <div class="fb-share-button" data-layout="button" data-href="//sharepoint.asia/hk"></div>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    <div id="page-wrapper">
        <?php
        include 'registersubmit.php';
        ?>

        <div class="row">
            <div class="col-lg-3 text-center">
            </div>
            <div class="col-lg-6 text-center">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form method="post" class="register" id="myFormToSubmitOptIn" role="form"
                              action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>'>
                            <div class="table-responsive">

                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Sign Up for JAMA Dermatology</th>
                                    </tr>
                                    </thead>

                                    <tr class="form-group">
                                        <td class="control-label">Email:&nbsp;</td>
                                        <td>
                                            <input name="email" value="<?php echo isset($email) ? $email : '' ?>"
                                                   type="text" id="email" class="form-control" maxlength="100"
                                                   placeholder="Email"/></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr class="form-group">
                                        <td class="control-label">First Name:&nbsp;</td>
                                        <td>
                                            <input name="firstname"
                                                   value="<?php echo isset($firstname) ? $firstname : '' ?>" type="text"
                                                   class="form-control" id="firstname" maxlength="100"
                                                   placeholder="First Name"/></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr class="form-group ">
                                        <td class="control-label">Last Name:&nbsp;</td>
                                        <td>
                                            <input name="lastname"
                                                   value="<?php echo isset($lastname) ? $lastname : '' ?>"
                                                   class="form-control" type="text" id="lastname" maxlength="100"
                                                   placeholder="Last Name"/></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr class="form-group">
                                        <td class="control-label">Opt-In:&nbsp;</td>
                                        <td>
                                            <input name="optin" type="checkbox" id="optin"/>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td><span id="reloadCaptcha" style="cursor: pointer;">
                                                <img src="images/refreshCaptcha.jpg" alt="Refresh Captcha"
                                                     title="reload"/></span><br/>
                                            <img id="myCaptchaImage" src="captcha.htm"/></td>
                                        <td>
                                            <input name="answer" class="form-control" type="text" id="answer" size="5"
                                                   maxlength="2" placeholder="Answer"/></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#" id="myResetButton" class="btn btn-primary btn-lg" role="button">Reset</a>


                                        </td>
                                        <td>
                                            <a href="#" id="submitButton" class="btn btn-primary btn-lg" role="button">Submit</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>


                        </form>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- JavaScript -->

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


</body>
</html>
