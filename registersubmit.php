<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
// define variables and set to empty values
    $email = $firstname = $lastname = "";
    $optin = false;
    $answer = -1;
    $emailValid = $firstnameValid = $lastnameValid = $answerValid = $IsFistTimeUser = false;

    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    $email = test_input($_POST["email"]);
    $firstname = test_input($_POST["firstname"]);
    $lastname = test_input($_POST["lastname"]);
    $optin = isset($_POST["optin"]);
    $answer = (int)test_input($_POST["answer"]);


    if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        $email = strtolower($email);
        $emailValid = true;
    }


    if (preg_match("/^.{3,100}$/", $firstname)) {
        $firstnameValid = true;
    }

    if (preg_match("/^.{3,100}$/", $lastname)) {
        $lastnameValid = true;
    }


    if (isset($_SESSION['rand_code'])) {
        if ($answer == (int)$_SESSION['rand_code']) {
            $answerValid = true;
        }

        require_once dirname(__FILE__) . '/internals/dbHandler.php';
        $db = new DbHandler();

        if ($emailValid) {
            $IsFistTimeUser = $db->isEmailNew($email);
        }
        echo "<div class=\"row\"> <div class=\"col-lg-12 text-center\">  <div class=\"panel-body\"> <div  class=\"alert alert-";
        if ($emailValid && $firstnameValid && $lastnameValid && $optin && $answerValid && $IsFistTimeUser) {

            $db->insertIntoTable($email, $firstname, $lastname);
            echo "success\"><strong>Well done " . $lastname . ", " . $firstname . " (" . $email . ") !</strong> You successfully Opted-In</div></div></div></div>";

            echo "<script>$(document).ready(function(){ setTimeout(function() { window.location.href = \"thankyou.htm?email=" . urlencode($email) . "&firstname=" . urlencode($firstname) . "&lastname=" . urlencode($lastname) . "\";}, 100);});</script>";


            unset($email, $firstname, $lastname);
        } else {
            echo "danger\"><strong>Error!</strong>";
            if (!$emailValid) {
                echo "<br/>Invalid email.";
            } else if (!$IsFistTimeUser) {
                echo "<br/>Duplicate email.";
            }
            if (!$firstnameValid) {
                echo "<br/>Invalid first name.";
            }

            if (!$lastnameValid) {
                echo "<br/>Invalid last name.";
            }

            if (!$optin) {
                echo "<br/>Mandatory to Opt-in.";
            }

            if (!$answerValid) {
                echo "<br/>Invalid Captcha answer.";
            }

            echo "</div></div></div></div>";

        }


    }


}


?>