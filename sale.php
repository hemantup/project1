<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Project #1 - Registration + Thank You Page">
    	<meta property="og:description" content="Project #1 - Registration + Thank You Page" />
	<meta property="og:title" content="Fort Hays State University Pharmaceutical Research and Manufacturers" />
<meta property="og:image" content="https://s3-us-west-2.amazonaws.com/asset.plexuss.com/college/logos/Fort_Hays_State_University.jpg" />
 <meta property="og:image:width" content="250" />
 <meta property="og:image:height" content="250" />
	
		<meta property="og:url" content="https://www.fhsu.edu/" />
		<meta property="og:site_name" content="Fort Hays State University Pharmaceutical Research and Manufacturers"/>
			  <meta name="classification" content="Project #1, Registration, Thank You Page" />
  <meta name="subject" content="Project #1 - Registration + Thank You Page" />
    <meta name="keywords" content="Project #1, Registration, Thank You Page" />
	 <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	 
		    <title>Fort Hays State University Pharmaceutical Research and Manufacturers</title>
			    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
				  <link href="metisMenu/dist/metisMenu.min.css" rel="stylesheet">
				  <link href="css/sb-admin-2.css" rel="stylesheet">
				    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	 </head>
	  
  <body ng-app="testingapp">
  <div id="fb-root"></div>
<script>


(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=502998669826426&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <div >

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-static-top" role="navigation" role="navigation" style="margin-bottom: 0">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="sale.htm">Fort Hays State University Pharmaceutical Research and Manufacturers</a>
		  
        </div>
		<ul class="nav navbar-top-links navbar-right">
		 <li >
              <a     href="//sharepoint.asia/hk" >About Us</a>
           </li>
<li class="dropdown">
<div class="fb-share-button" data-layout="button" data-href="//sharepoint.asia/hk"  ></div></li>
</ul>
 <div class="navbar-inverse sidebar" role="navigation">
 <div class="sidebar-nav navbar-collapse">
 
   <ul class="nav" id="side-menu">
     <li class="sidebar-search">
                           
                            <!-- /input-group -->
                        </li>
   
  
     <li> <script type="text/javascript" src="http://je.revolvermaps.com/2/1.js?i=46q2wxj4e8j&amp;s=220&amp;m=0&amp;v=true&amp;r=false&amp;b=000000&amp;n=false&amp;c=ff0000" async="async"></script>
  </li>
   <li><div class="fb-like" data-href="https://www.facebook.com/AuctionCell" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div></li>
<li><a rel="external nofollow"  target="_blank"  href="http://info.flagcounter.com/hHTJ"><img width=130.2 height=112 rel="external nofollow"  src="http://s04.flagcounter.com/count/hHTJ/bg_FFFFFF/txt_000000/border_CCCCCC/columns_2/maxflags_12/viewers_0/labels_1/pageviews_1/flags_1/" alt="Flag Counter" border="0"></a></li>

 

		   <li ><div class="fb-share-button" data-layout="button" data-href="//sharepoint.asia/hk"  ></div></li>
		 
 </ul>
 </div>
  </div>
      </nav>

      <div id="page-wrapper">

	
     
        <div class="row">
          <div class="col-lg-4 text-center">
            <div class="panel panel-default">
          <div class="panel-body">
			  
			<img src="images/Pills-and-Capsules.jpg" alt="Smiley face" >
				
         </div>   
              </div>
                  
          </div>
          <div class="col-lg-4 text-center">
           <div class="panel panel-default">
              <div class="panel-body">
        <img src="images/story.png" alt="Story" >
         </div>
            </div>            
          </div>
          <div class="col-lg-4 text-center">
           <div class="panel panel-default">
              <div class="panel-body">
			  
		 <img src="images/better.png" alt="Better" >
				
         </div>            
          </div></div>
        </div><!-- /.row -->
		
		       <div class="row">
          <div class="col-lg-12 text-center">
            <div class="panel panel-default">
          <div class="panel-body">
			  
			NDIANAPOLIS, March 7, 2016 /PRNewswire/ -- Eli Lilly and Company (NYSE: LLY) announced today that JAMA Dermatology has published detailed results from three pivotal Phase 3 trials that evaluated the effect of ixekizumab on work productivity in patients with moderate-to-severe plaque psoriasis. Specific results from the UNCOVER-1 study were also presented Monday during the American Academy of Dermatology (AAD) Annual Meeting in Washington, D.C.

In an analysis of the UNCOVER-1, UNCOVER-2 and UNCOVER-3 studies, the effect of ixekizumab on work productivity was evaluated by the change from baseline as measured by Work Productivity and Activity Impairment-Psoriasis (WPAI-PSO) scores at 12 weeks. The validated, self-reported WPAI questionnaire is used to measure impairment of work activities due to general health or a specific condition.1

In all three studies, patients treated with ixekizumab reported improved work productivity compared to patients treated with placebo. In UNCOVER-1, improvements in work productivity were also sustained up to 60 weeks in those who demonstrated initial clinical response to ixekizumab at 12 weeks.

"Psoriasis is a serious, systemic disease that can have a significant impact on a patient's overall health and quality of life, including work productivity and overall activity levels," said April Armstrong, M.D., MPH, corresponding study author and associate dean of clinical research at Keck School of Medicine of University of Southern California. "The results presented at AAD and detailed in JAMA Dermatology further reinforce published data supporting ixekizumab as a potential treatment for moderate-to-severe plaque psoriasis. If approved, ixekizumab may provide dermatologists with a new option to address both skin symptoms and health-related outcomes for patients, including work-related activities."
		<a href="register.htm"><img src="images/thin-gray-signup-button-hi.png" alt="Signup" /> </a>	 
				
         </div>   
              </div>
                  
          </div>
                  
          </div></div>
        </div><!-- /.row -->
	
      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



  </body>
</html>
